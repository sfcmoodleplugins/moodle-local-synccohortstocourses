<?php


// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * cohort sync with LDAP groups script.
 *
 * This script is meant to be called from a cronjob to sync moodle's cohorts
 * registered in LDAP groups where the CAS/LDAP backend acts as 'master'.
 *
 * Sample cron entry:
 * # 5 minutes past 4am
 * 5 4 * * * $sudo -u www-data /usr/bin/php /var/www/moodle/auth/cas/cli/sync_cohorts.php
 *
 * Notes:
 *   - it is required to use the web server account when executing PHP CLI scripts
 *   - you need to change the "www-data" to match the apache user account
 *   - use "su" if "sudo" not available
 *   - If you have a large number of users, you may want to raise the memory limits
 *     by passing -d memory_limit=256M
 *   - For debugging & better logging, you are encouraged to use in the command line:
 *     -d log_errors=1 -d error_reporting=E_ALL -d display_errors=0 -d html_errors=0
 *
 *THis script should be run some time after /var/www/moodle/auth/cas/cli/sync_users.php
 *
 * Performance notes:
 * We have optimized it as best as we could for PostgreSQL and MySQL, with 27K students
 * we have seen this take 10 minutes.
 *
 * @package    auth
 * @subpackage CAS 
 * @copyright  2010 Patrick Pollet - based on code by Jeremy Guittirez
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


/**
 * CONFIGURATION
 * this script make use of current Moodle's LDAP/CAS settings 
 * user_attribute 
 * member_attribute
 * member_attribute_isdn
 * objectclass
 * 
 * and the following default values that can be altered in settings page
 * group_attribute='cn';          // in case your groups are not cn=xxx,
 * group_class='groupofuniquenames'; // in case your groups are of class group ...
 * real_user_attribute='uid';     // in case your user_attribute is in mixed case in LDAP (sAMAccountName) 
 * process_nested_groups=0;       // turn on nested groups
 * debug_ldap_groupes=false;           // turn on extensive debug upon running
 * cohort_synching_ldap_groups_autocreate_cohorts // if false will not create missing cohorts (admin must create them before) 
 * 
 * 
 */

/**
 * REVISIONS
 * 1.0 initial release see
 * 1.1 1 Nov 2012 added support for the case when group member attribute IS NOT of
 * the form xx=moodleusername,ou=yyyy,ou=zzzz that seems to be more commun than I thought ...
 * i.e. in AD it seems to be cn=user fullname, ou=yyyy,ou=zzzz
 * 1.2 16 Nov 2012 added support for nested groups
 * 1.2.1 18 Nov 2012 improved for LDAP directories with mixed case attributes names (i.e. sAMMAccountName)
 *                   and values (i.e. username=JDoe)
 * 1.3   07 March 2013 also work when authentication is LDAP and not CAS                   
 */

define('CLI_SCRIPT', true);

require (dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');

require_once("$CFG->dirroot/group/lib.php");
require_once("$CFG->dirroot/enrol/cohort/locallib.php");

//require (dirname(dirname(__FILE__)) . '/locallib.php');


// Ensure errors are well explained
$CFG->debug = DEBUG_NORMAL;

$starttime = microtime();

$enrol = enrol_get_plugin('cohort');


        global $DB;

        $sql = " SELECT u.id,u.name
                          FROM {cohort} u
			WHERE u.contextid=1";
        $cohortsdb= $DB->get_records_sql($sql);




foreach ($cohortsdb as $cohort=>$cohorts) {
	$sql = " SELECT u.id,u.idnumber
            FROM {course} u
			WHERE FIND_IN_SET(:courseidnumber,u.idnumber)";
	$qualificationCode = implode('-',array_slice(explode('-', $cohorts->name, 3),0,1));
	$courseCode = implode('-',array_slice(explode('-', $cohorts->name, 3),1,1));
	$params['courseidnumber'] = $qualificationCode . '-' . $courseCode;
	$coursesdb = $DB->get_records_sql($sql, $params);

	//print_r($params);

	foreach ($coursesdb as $course=>$courses) {

	        $sql = " SELECT u.id
                          FROM {enrol} u
                         JOIN {cohort} cm ON (cm.id = u.customint1)
                        WHERE u.courseid = :courseid AND cm.id = :cohortid";
		$params['courseid'] = $courses->id;
		$params['cohortid'] = $cohorts->id;
		$cohortexists = $DB->get_records_sql($sql, $params);
		//print_r($cohortexists);

		if (empty($cohortexists)) {

			$course = $DB->get_record('course', array('id'=>$courses->id), '*', MUST_EXIST);

			print "adding " . $cohorts->name . " to " . $course->fullname . PHP_EOL;

			$groupid = enrol_cohort_create_new_group($course->id, $cohorts->id);

			$enrol->add_instance($courses, array('name' => $cohorts->name, 'status' => 0,
			'customint1' => $cohorts->id, 'roleid' => 5, 'customint2' => $groupid));



			//print_r($courses);
			//print_r($instance);

			$trace = new null_progress_trace();
			enrol_cohort_sync($trace, $courses->id);
			$trace->finished();
		}

	}




}




$difftime = microtime_diff($starttime, microtime());
print("Execution took ".$difftime." seconds".PHP_EOL);
